﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.Reflection;
using System;


public enum ListType 
{
	VerticalMultipleList,HorizontalMultipleList
}
public enum MenuState{
	Main, Confirmation, Ask

}
public class UIGenerate : MonoBehaviour {
	DataManager dataManager;
	MenuState menuState;
	MenuState previousMenuState;
	[SerializeField]
	Transform SkillPanel, AskPanel, ConfrmationPanel;
	// Use this for initialization
	void Start () {
		menuState = MenuState.Main;
		dataManager = DataManager.Instance;
		LoadUiElements (dataManager.SkillsList, SkillPanel, ListType.VerticalMultipleList);
		GenerateUiElements (dataManager.SkillsList, SkillPanel, ListType.VerticalMultipleList);

	}


	public void LoadUiElements <T>(List<T> data, Transform panel, ListType listType)where T:IBoughtable{
		GameObject list = (GameObject) Instantiate(Resources.Load<GameObject>("Prefabs/UI/"+listType.ToString()));
		list.name = listType.ToString ();
		list.transform.SetParent(panel.transform);
		list.transform.position = panel.transform.position;
		list.transform.localScale = new Vector3 (1, 1, 1);
		Transform listItemParent = list.transform.Find ("List");
		int i = 0;
		foreach (var item in data) {
			i += 1;
			GameObject listItem = (GameObject) Instantiate(listItemParent.Find("Item").gameObject);
			listItem.transform.SetParent(listItemParent.transform);
			listItem.transform.position = listItemParent.transform.position;
			listItem.transform.localScale = new Vector3 (1, 1, 1);
			listItem.name = i.ToString ();
			var props = item.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance ).Where(
				prop => CustomAttribite.IsDefined(prop, typeof(CustomAttribite)));
			Transform properitesList = listItem.transform.Find ("PropertiesContent").Find ("List");
//			foreach (var oProp in propsBase) {
//				GameObject listProp = (GameObject) Instantiate(Resources.Load<GameObject>("Prefabs/"+"Prop"));
//				listProp.transform.SetParent(properitesList.transform);
//				listProp.transform.position = properitesList.transform.position;
//				listProp.transform.localScale = new Vector3 (1, 1, 1); 
//			}
			int j=0;
//			Transform TitleText =listItem.transform.Find ("TitlePanel").Find ("TitleText");
			foreach (var oProp in props) {
				j += 1;
				CustomAttribite attr = (CustomAttribite) oProp.GetCustomAttributes (typeof(CustomAttribite), true).FirstOrDefault (); 
				if (attr.customAttrType == CustomAttrType.Info) {
					
				
					GameObject listProp = (GameObject)Instantiate (properitesList.Find ("Prop").gameObject);
					listProp.name = "Prop";
					listProp.transform.SetParent (properitesList.transform);
					listProp.transform.position = properitesList.transform.position;
					listProp.transform.localScale = new Vector3 (1, 1, 1); 
					listProp.name = attr.name;
					listProp.transform.Find ("PropName").Find ("PropNameText").GetComponent<Text> ().text = attr.name;
					listProp.transform.Find ("PropImage").GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Prefabs/UI/" + oProp.Name);
				}
					if (j>=props.Count()) {
					Destroy (properitesList.Find ("Prop").gameObject);
				}

			}

			//todo zmienic tak zeby duplikowalo podczas generowania pioerwszy  zlisty propsow oraz itemow i tak wew sztsykim. Czyli minimum 1 zawsze elementy wq  liscie musi byc
//			var props = item.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where(
//				prop => CustomProperty.IsDefined(prop, typeof(CustomProperty)));
//			Debug.Log ("props ; " + props.Count ());
		}
		if (i>=data.Count) {
			Destroy (listItemParent.Find ("Item").gameObject);
		}

	}

	public void GenerateUiElements<T>(List<T> data, Transform panel, ListType listType)where T:IBoughtable{
		int i = 0;
		foreach (var item in data) {
			i += 1;
			Transform TitleText = panel.Find(listType.ToString()).Find ("List").Find (i.ToString ()).Find ("TitlePanel").Find ("TitleText");
			Transform Lock = panel.Find (listType.ToString ()).Find ("List").Find (i.ToString ()).Find ("Lock");
			Button btnBuy = panel.Find (listType.ToString ()).Find ("List").Find (i.ToString ()).Find("BuyButton").GetComponent<Button>();
			btnBuy.onClick.RemoveAllListeners ();
			var itemCopy = item;
			Lock.gameObject.SetActive (false);
			btnBuy.gameObject.SetActive (false);
//			Debug.Log ("item.IsBought  : " + item.IsBought);
			if (!itemCopy.IsBought) {
				Lock.gameObject.SetActive (true);
				btnBuy.gameObject.SetActive (true);
				btnBuy.onClick.AddListener (() => {
					OpenAskPanelForBuy(itemCopy,"Are you sure you want to buy it for " + itemCopy.Price + "?",data,panel,listType);
//					GenerateUiElements (dataManager.SkillsList, SkillPanel, ListType.VerticalMultipleList);
				});
//				btnBuy
			}

			var props = item.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance ).Where(
				prop => CustomAttribite.IsDefined(prop, typeof(CustomAttribite)));

			foreach (var oProp in props) {
//				Debug.Log ("oProp.Name ; " + oProp.Name);
				CustomAttribite attr = (CustomAttribite) oProp.GetCustomAttributes (typeof(CustomAttribite), true).FirstOrDefault (); 
				if (attr.customAttrType == CustomAttrType.Title) {
					TitleText.GetComponent<Text> ().text = oProp.GetValue (item).ToString ();
				} if(attr.customAttrType == CustomAttrType.Info) {
//					Debug.Log ("I : " + panel.Find (listType.ToString ()).Find ("List").Find (i.ToString ()).Find ("PropertiesContent").Find ("List").Find ("Prop").name);
					Transform goProperty = panel.Find (listType.ToString ()).Find ("List").Find (i.ToString ()).Find ("PropertiesContent").Find ("List").Find (attr.name);
					goProperty.Find ("PropValue").Find ("PropValueText").GetComponent<Text> ().text = oProp.GetValue (item).ToString ();
				}
			}
		}

	}

	public void OpenAskPanelForBuy<T>(T item, string msg,List<T> data, Transform panel, ListType listType)where T:IBoughtable{
//		ConfirmationPanel.gameObject.SetActive (true);
		ChangeMenuPanel(MenuState.Ask);
		Button btnYes = AskPanel.transform.Find ("btnYes").GetComponent<Button> ();
		Button btnNo = AskPanel.transform.Find ("btnNo").GetComponent<Button> ();
		Text txtMessage =AskPanel.transform.Find ("txtMessage").GetComponent<Text> ();
		txtMessage.text = msg;
		btnYes.onClick.RemoveAllListeners ();
		btnNo.onClick.RemoveAllListeners ();
		btnNo.onClick.AddListener (() => {
			ChangeMenuPanel(MenuState.Main);
		});
		btnYes.onClick.AddListener (() => {
			bool isBuySuccess = item.Buy ();
			Debug.Log("isBuySuccess : " +isBuySuccess);
			if (isBuySuccess) {
				ChangeMenuPanel(MenuState.Main);
				GenerateUiElements (data, panel, listType);
//				ConfirmationPanel.gameObject.SetActive (false);
			}
			else {
				OpenConfirmationPanel("You cant buy it right now!");
			}
		});

	}

	public void OpenConfirmationPanel(string msg){
		//		ConfirmationPanel.gameObject.SetActive (true);
		ChangeMenuPanel(MenuState.Confirmation);
		Button btnOk = AskPanel.transform.Find ("btnOk").GetComponent<Button> ();
		Text txtMessage =AskPanel.transform.Find ("txtMessage").GetComponent<Text> ();
		txtMessage.text = msg;
//		btnOk.gameObject.SetActive (false);
		btnOk.onClick.RemoveAllListeners ();

		btnOk.onClick.AddListener (() => {
				ChangeMenuPanel(MenuState.Main);
		});

	}

	public void ChangeMenuPanel(MenuState newMenuState){
		previousMenuState = menuState;
		menuState = newMenuState;
		AskPanel.gameObject.SetActive (false);
		ConfrmationPanel.gameObject.SetActive (false);

		switch (menuState) {
		case	MenuState.Ask:
			AskPanel.gameObject.SetActive (true);
			break;
		case	MenuState.Confirmation:
			ConfrmationPanel.gameObject.SetActive (true);
			break;	
		default:
			break;
		}


	}

}
//easse ui