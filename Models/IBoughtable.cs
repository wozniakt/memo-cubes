﻿using UnityEngine;
using System.Collections;

public interface IBoughtable  {

	bool Buy();
	 bool IsBought {
		get;
		set;
	}
	long Price {
		get;
		set;
	}
}
