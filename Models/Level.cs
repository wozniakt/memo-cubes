﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Level 
{

	[SerializeField]
	List<Color> colorsList;
	public List<Color> ColorsList {
		get{ return colorsList; }
		set {
			colorsList = value;
		}
	}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked {
		get{ return isUnlocked; }
		set {
			isUnlocked = value;
		}
	}

	[SerializeField]
	int id;
	public int ID {
		get{ return id; }
		set {
			id = value;
		}
	}




	[SerializeField]
	int unlockHighscore;
	public int UnlockHighscore {
		get{ return unlockHighscore; }
		set {
			unlockHighscore = value;
		}
	}

	[SerializeField]
	int pointsMulitipler;
	public int PointsMulitipler {
		get{ return pointsMulitipler; }
		set {
			pointsMulitipler = value;
		}
	}

}

