﻿using UnityEngine;
using System.Collections;

public abstract class Item:IBoughtable
{


	[SerializeField]
	public int Id;

	[SerializeField]
	[CustomAttribite(name="Title", customAttrType= CustomAttrType.Title)]
	public string Title;

	[SerializeField]
	[CustomAttribite(name="Price", customAttrType= CustomAttrType.Info)]
	public long price;
	public long Price{
		get {return price;}
		set{price = value;}
	}

	[SerializeField]
	public bool isBought;
	public bool IsBought {
		get {return isBought;}
		set{isBought = value;}
	}

	[SerializeField]

	public string Description;

	public bool Buy( ){
		bool result = false;
		if (IsBought) {
			result= false;
		}
		IsBought = true;
		result = true;
		return result;
	}



}

