﻿using UnityEngine;
using System.Collections;


public enum GameState {Menu, GameOn, GamePaused,GameRestart_AfterPause,GameLost, GameRestart_AfterLost, GameResumed_AfterPause, GameQuit, Distraction}


public enum EffectType{Explosion1}
public enum SoundType{ Explode, Ticking, laugh, Alarm,Scream}