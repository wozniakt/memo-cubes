﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DataManager : MonoBehaviour
{

	GlobalEventsManager globalEventsManager;
	public static DataManager Instance;
	public List<SkillModel> SkillsList;
	public List<Level> LevelsList;
	//public static DataManager instance;
	public GameState oGameState;
	public PlayerData oPlayerData;
	public int currentCubesCount;

	// Use this for initialization
	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (Instance.gameObject);
	}

	void Start(){
		oPlayerData = new PlayerData ();
		//PlayerPrefs.SetInt ("highscorePointsCount", 0);
		getPlayerData ();
		oPlayerData.Points = 0;
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameState;

	}
	void setPlayerData(){
		PlayerPrefs.SetInt ("CoinsCount", oPlayerData.Points);
	}


	void getPlayerData(){
		oPlayerData.HighscorePointsCount=PlayerPrefs.GetInt ("highscorePointsCount", 0);
		foreach (Level oLevel in LevelsList) {
			if (oLevel.UnlockHighscore<=oPlayerData.HighscorePointsCount) {
				oLevel.IsUnlocked = true;
			}
		}
	}


	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ChangeGameState;
	}

	public void  ChangeGameState (GameState gameState)
	{
//		Debug.Log ("CHNGE DATAMANAGR STATE: " +gameState);
		oGameState = gameState;
		Time.timeScale = 1;
		oPlayerData.HighscorePointsCount=oPlayerData.HighscorePointsCount;
		switch (oGameState) {
		case GameState.Menu:
			currentCubesCount = 0;

			break;
		case GameState.GameOn:
			ColorsManager.instance.InitColorsInGame ();
			break;
		case GameState.GameQuit:
			break;
		case GameState.GamePaused:
			Time.timeScale = 0;
			break;
		case GameState.Distraction:
			Time.timeScale = 1;
			break;

		case GameState.GameResumed_AfterPause:
			//oGameState = GameState.GameOn;
			break;
		case GameState.GameRestart_AfterPause:
			//oGameState = GameState.GameOn;
			break;
		case GameState.GameLost:
			currentCubesCount = 0;
			//Time.timeScale = 0.001f;
			break;
		case GameState.GameRestart_AfterLost:
			//oGameState = GameState.GameOn;
			break;
		}
	}


}
