﻿using UnityEngine;
using System.Collections;

public class SoundsManager : MonoBehaviour {
	public static SoundsManager Instance;
	public AudioClip[] sounds;
	public AudioSource currentAudioSource;
	public AudioSource AudioSource2, AudioSource3,AudioSource4,AudioSource5;
	// Use this for initialization
	void Awake () {

		if (Instance == null)
			Instance = this;
		else if (Instance != this)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(Instance.gameObject);
	}

	public void MuteAll(){
		if (!AudioListener.pause) {
			AudioListener.pause = true;
			AudioListener.volume = 0;
		} else {
			AudioListener.pause = false;
			AudioListener.volume = 1;

		}
	}



	AudioClip GetSound(SoundType soundType){

		for (int i = 0; i < sounds.Length; i++) {
			if (sounds[i].name == soundType.ToString()) {
				return sounds [i];
			}
		}
		return null;
	}

	public void StartLoopSound(SoundType soundType){
		StartCoroutine (PlaySoundLoop (soundType));
	}

	public IEnumerator PlaySoundLoop(SoundType soundType){
		if (!currentAudioSource.isPlaying) {
			currentAudioSource.clip = GetSound (soundType);
			currentAudioSource.Play ();
		}
		yield return new WaitForSeconds (currentAudioSource.clip.length-2);
		StartCoroutine (PlaySoundLoop (soundType));
	}


	public void PlaySound(SoundType soundType){
		if (!currentAudioSource.isPlaying) {
			currentAudioSource.clip = GetSound (soundType);
			currentAudioSource.Play ();
		} else if (!AudioSource2.isPlaying) { 
			AudioSource2.clip = GetSound (soundType);
			AudioSource2.Play ();
		} else if (!AudioSource3.isPlaying) { 
			AudioSource3.clip = GetSound (soundType);
			AudioSource3.Play ();
		} 
		else if (!AudioSource4.isPlaying) { 
			AudioSource4.clip = GetSound (soundType);
			AudioSource4.Play ();
		} else if (!AudioSource5.isPlaying) { 
			AudioSource5.clip = GetSound (soundType);
			AudioSource5.Play ();
		}
	}




}
