﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorsManager : MonoBehaviour
{
	public static ColorsManager instance;

	public Color currentColor;
	public List<Color> colorsList;
	public List<Color> fakeColorsList;
	GlobalEventsManager globalEventsManager ;

	public int counter;
	// Use this for initialization
	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start ()
	{
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ResetCurrentColor;

	}


	public void InitColorsInGame(){
		colorsList = new List<Color> ();
		CreateTargetColorsList ();
		CreateFakeColorsList ();
		ColorsManager.instance.ResetCurrentColor (GameState.GameLost);
	}

	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ResetCurrentColor;
	}
	
	// Update is called once per frame
	void CreateTargetColorsList ()
	{
//		Debug.Log ("!!!!!!!!!!!!!!!!! " + DataManager.Instance.oPlayerData.CurrentLevel);
		foreach (Color oColor in DataManager.Instance.oPlayerData.CurrentLevel.ColorsList) {
			colorsList.Add (oColor);
		}
//		colorsList.Add (Color.red);
//		colorsList.Add (Color.green);
//		colorsList.Add (Color.blue);
	}

	void CreateFakeColorsList ()
	{
		//fakeColorsList.Add (Color.yellow);
		fakeColorsList.Add (Color.black);
		//fakeColorsList.Add (Color.black);
	}

	void ResetCurrentColor(GameState oGameState){
		if (oGameState==GameState.GameLost) {
			currentColor = colorsList [0];
			counter = 0;
		}

	}

	//todo - lista z kolorami które się powtarzają
	public void changeCurrentColor(){
		if ( counter!=colorsList.Count-1) {
			counter++;
		currentColor = colorsList [counter];
		} else if ( counter ==colorsList.Count-1){
			currentColor = colorsList [0];
		counter=0;
			}
//		counter++;
//		currentColor = colorsList [counter];

	}
}

