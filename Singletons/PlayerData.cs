﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData 
{

	[SerializeField]
	float comboCount;
	public float ComboCount
	{
		get
		{return comboCount;}
		set{

			comboCount= value;

		}
	}

	[SerializeField]
	Level currentLevel;
	public Level CurrentLevel
	{
		get
		{return currentLevel;}
		set{

			currentLevel= value;

		}
	}

//	[SerializeField]
//	int pointsCount;
//	public int PointsCount
//	{
//		get
//		{return pointsCount;}
//		set{
//
//			pointsCount= value;
//
//		}
//	}

	[SerializeField]
	int highscorePointsCount;
	public int HighscorePointsCount
	{
		get
		{
			highscorePointsCount= Mathf.Max( highscorePointsCount, pointsCount);
			return highscorePointsCount;}
		set{
			//if (highscorePointsCount<value) {
		
				highscorePointsCount= Mathf.Max( value, pointsCount);
			//}
			PlayerPrefs.SetInt ("highscorePointsCount", highscorePointsCount);
			//Debug.Log("HI "+highscorePointsCount);
		}
	}


	[SerializeField]
	int pointsCount;
	public int Points
	{
		get
		{return pointsCount;}
		set{
			if (value>0) {
				pointsCount= value;
			} else {
				pointsCount = 0;		
			}	
			PlayerPrefs.SetInt ("Points", Points);
		}
	}

}