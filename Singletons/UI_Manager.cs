﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


public class UI_Manager : MonoBehaviour
{
	GlobalEventsManager globalEventsManager;
	public GameObject ScaryPanel;
	public Button Button_Menu_Start, Button_Menu_Quit;
	public Button  Button_Pause,Button_Pause_Restart, Button_Pause_BackToMenu, Button_Pause_Resume;
	public Button  Button_GameOver_Restart, Button_GameOver_Quit, Button_GameOver_GoToMenu;

	public GameObject Panel_Hud, Panel_Gameplay;
	public GameObject Panel_GameOver, Panel_Pause, Panel_Menu;

	public static string PREFAB_PATH = "Prefabs/";
	public Text pointsText;

	public static UI_Manager instance;
	public GameState oGameState;
	public GameObject mainCamera;

	public Image imgTime;
	public float counterTime,timeToEnableScaryPanel;

	GameObject gamePlayPanel;
	public GameObject gamePlayPlaceHolder;
	public GameObject LevelsPlaceHolderInScroll;
	int counter;
	Color tmpColor;
	public  Text txtHighscore;

	Animator Img_RandomEvent_Animator,PanelBackground_Animator;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
		counterTime = 1;
	}

	void Start(){
		counterTime = 1;
		AddListenersToButtons ();
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints += this.UpdateHud;
		globalEventsManager.TriggerOnChangeGameState (GameState.Menu);
		GenerateUI ();
		txtHighscore.text = DataManager.Instance.oPlayerData.HighscorePointsCount.ToString();
		//globalEventsManager.TriggerGetPoints (0);
	}




// Update is called once per frame
//	IEnumerator enableScaryPanel ()
//{
//		if (oGameState==GameState.GameOn) {
//		yield return new WaitForSeconds (timeToEnableScaryPanel);
//		SoundsManager.Instance.PlaySound (SoundType.Scream);
//		ScaryPanel.gameObject.SetActive (true);
//		yield return new WaitForSeconds (0.2f);
//		ScaryPanel.gameObject.SetActive (false);
//
//			
//		}
//		StartCoroutine (enableScaryPanel ());
//
//}


	void Update(){
		//Debug.Log (DataManager.Instance.oGameState);
		if (DataManager.Instance.oGameState!=GameState.Menu && DataManager.Instance.oGameState!=GameState.GameLost) {
			
			counterTime = (counterTime -Time.deltaTime/5);
			imgTime.fillAmount = counterTime;
		if (counterTime<=0) {
				SoundsManager.Instance.PlaySound (SoundType.Alarm);
			globalEventsManager.TriggerOnChangeGameState (GameState.GameLost);
		}
		}
	}
	void OnDisable(){

		globalEventsManager.OnChangeGameState -= this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints -= this.UpdateHud;
	}

	void GenerateUI(){
		int counterLevelHolder = 0;
		foreach (Level oLevel in DataManager.Instance.LevelsList) {
			counter = 0;
			GameObject LevelHolderUI = (GameObject)Instantiate (Resources.Load ("Prefabs/UI/" + "LevelHolderUI"));
			LevelHolderUI.transform.SetParent (LevelsPlaceHolderInScroll.transform);
			LevelHolderUI.transform.localScale = new Vector3 (1, 1, 1);
			LevelHolderUI.transform.position = LevelsPlaceHolderInScroll.transform.position;
			LevelHolderUI.name = "LevelHolderUI" + counterLevelHolder;

			counterLevelHolder++;
			foreach (Color oColor in oLevel.ColorsList) {
				counter = counter + 1;
				GameObject cubeUI = (GameObject)Instantiate (Resources.Load ("Prefabs/UI/" + "ColorCubeUI"));
				cubeUI.SetActive (true);
				//cubeUI.transform.localScale = new Vector3 (0.1f,0.1f, 0.1f);
				tmpColor.a = 1;
				cubeUI.GetComponent<Image> ().color = new Color (oColor.r, oColor.g, oColor.b, 1);
				cubeUI.transform.SetParent (LevelHolderUI.transform);
				cubeUI.transform.localPosition = new Vector3 (LevelHolderUI.transform.GetComponent<RectTransform> ().rect.width / (oLevel.ColorsList.Count + 1) * (counter),
				LevelHolderUI.transform.localPosition.y, LevelHolderUI.transform.position.z);
				//Debug.Log (LevelHolderUI.transform.GetComponent<RectTransform>().rect.width/cubeUI.transform.GetComponent<RectTransform>().rect.width);
//				Debug.Log (LevelHolderUI.transform.GetComponent<RectTransform>().rect.width/oLevel.ColorsList.Count*counter);
			}

			GameObject textLock = (GameObject)Instantiate (Resources.Load ("Prefabs/UI/" + "TextLock"));
//			LevelHolderUI.GetComponentInChildren<Text> ().enabled = !oLevel.IsUnlocked;
			textLock.transform.SetParent (LevelHolderUI.transform);
			textLock.transform.localPosition= (LevelHolderUI.transform.localPosition);
			LevelHolderUI.GetComponentInChildren<Text> ().enabled = !oLevel.IsUnlocked;
			textLock.GetComponentInChildren<Text> ().text = "UNLOCK WITH HIGHSCORE " + oLevel.UnlockHighscore;
			txtHighscore.text = DataManager.Instance.oPlayerData.HighscorePointsCount.ToString ();
		}
	}

	void UpdateLevelsUI(){

		int outputConvert;
		GameObject[] LevelHolderUIs = GameObject.FindGameObjectsWithTag ("LevelHolder");
	//	Debug.Log (LevelHolderUIs.Length.ToString());
		foreach (GameObject oLevelHolderUI in LevelHolderUIs) {
			
			foreach (Level oLevel in DataManager.Instance.LevelsList) {
				Int32.TryParse (oLevelHolderUI.name.Substring (13),out outputConvert);

				if (DataManager.Instance.LevelsList.IndexOf(oLevel)==  outputConvert) {
//					Debug.Log (DataManager.Instance.LevelsList.IndexOf(oLevel)+" "+outputConvert);
					oLevel.IsUnlocked = (DataManager.Instance.oPlayerData.HighscorePointsCount >= oLevel.UnlockHighscore);
					oLevelHolderUI.GetComponentInChildren<Text> ().enabled = !oLevel.IsUnlocked;
				}
			}
		}
	}


	void ChangeGameStateUI(GameState gameStat){
		StartCoroutine (ChangeGameStateUI_coroutine( gameStat));
	}


	public IEnumerator  ChangeGameStateUI_coroutine(GameState gameState)
	{ 
//		Debug.Log ("CHNGE UI STATE  " +gameState);
		Panel_Menu.SetActive (false); 
//		Panel_Gameplay.SetActive (false); 
		Panel_Pause.SetActive (false);  
		Panel_GameOver.SetActive (false); 
		//Panel_Hud.SetActive (false);
		//mainCamera.GetComponent<Camera> ().fieldOfView = 60;
		oGameState = gameState;
//		Debug.Log (oGameState);
		//SoundsManager.Instance.PlaySound (SoundType.Ticking);
		switch (oGameState) {
		case GameState.Menu:
			counterTime = 1;
			DataManager.Instance.oPlayerData.HighscorePointsCount = Mathf.Max (DataManager.Instance.oPlayerData.HighscorePointsCount, DataManager.Instance.oPlayerData.Points);
			txtHighscore.text = DataManager.Instance.oPlayerData.HighscorePointsCount.ToString ();
			DistractionManager.instance.panelDistraction.SetActive (false);	
		
			Panel_Hud.SetActive (false);
			Panel_Gameplay.SetActive (false); 
			//mainCamera.GetComponent<Camera> ().cullingMask = -1;
			Panel_Menu.SetActive (true); 
			GameplayContainerActive (false);
			UpdateLevelsUI ();
			break;
		case GameState.GameOn:
//			StartCoroutine (enableScaryPanel ());
		 SoundsManager.Instance.StartLoopSound (SoundType.Ticking);
			DataManager.Instance.oPlayerData.Points = 0;
			UpdateHud (0);
			mainCamera.transform.localPosition = mainCamera.GetComponent<RandomizePosition> ().initialPosition;
			mainCamera.transform.eulerAngles = mainCamera.GetComponent<RandomizePosition> ().initialRotation;
			counterTime = 1;
			Panel_Gameplay.SetActive (true); 
			Panel_Menu.SetActive (false); 
			GameplayContainerActive (true);
			Panel_Hud.SetActive (true);
			break;
		case GameState.GameQuit:
			Application.Quit();
			break;
		case GameState.GamePaused:
			//mainCamera.transform.localPosition = mainCamera.GetComponent<RandomizePosition> ().initialPosition;
			//mainCamera.transform.eulerAngles = mainCamera.GetComponent<RandomizePosition> ().initialRotation;
			Panel_Pause.SetActive (true);
			//mainCamera.GetComponent<Camera> ().cullingMask = -2;
			break;

		case GameState.Distraction:
			DistractionManager.instance.panelDistraction.SetActive (true);	
			//mainCamera.GetComponent<Camera> ().cullingMask = -2;
			break;
		case GameState.GameResumed_AfterPause:
			//mainCamera.GetComponent<Camera> ().cullingMask =-1;
			Panel_Pause.SetActive (false); 
			Panel_Hud.SetActive (true);
			break;
		case GameState.GameRestart_AfterPause:
			DistractionManager.instance.panelDistraction.SetActive (false);	
			GameplayContainerActive (false);
			Panel_Gameplay.SetActive (false); 
			Panel_Pause.SetActive (false); 
			Panel_Hud.SetActive (true);
			mainCamera.transform.localPosition = mainCamera.GetComponent<RandomizePosition> ().initialPosition;
			mainCamera.transform.eulerAngles = mainCamera.GetComponent<RandomizePosition> ().initialRotation;
			GameplayContainerActive (true);
			Panel_Gameplay.SetActive (true); 
			counterTime = 1;
			DataManager.Instance.oPlayerData.Points = 0;
			UpdateHud (0);
			break;
		case GameState.GameLost:
//			Debug.Log ("UI ");
			DistractionManager.instance.panelDistraction.SetActive (false);	

			yield return new WaitForSeconds(1);

			//mainCamera.GetComponent<Camera> ().fieldOfView = 1;\
			mainCamera.transform.localPosition = mainCamera.GetComponent<RandomizePosition> ().initialPosition;
			mainCamera.transform.eulerAngles = mainCamera.GetComponent<RandomizePosition> ().initialRotation;
			GameplayContainerActive (false);
			Panel_GameOver.SetActive (true);
			Panel_Hud.SetActive (true);
			break;
		case GameState.GameRestart_AfterLost:
			DistractionManager.instance.panelDistraction.SetActive (false);	
			mainCamera.transform.localPosition = mainCamera.GetComponent<RandomizePosition> ().initialPosition;
			mainCamera.transform.eulerAngles = mainCamera.GetComponent<RandomizePosition> ().initialRotation;
			counterTime = 1;
			Panel_Gameplay.SetActive (false); 
			Panel_Hud.SetActive (true);
			Panel_GameOver.SetActive (false);
			GameplayContainerActive (false);
			GameplayContainerActive (true);
			Panel_Gameplay.SetActive (true); 
			DataManager.Instance.oPlayerData.Points = 0;
			UpdateHud (0);
			break;
		}
	}

	void AddListenersToButtons(){
		Button_Menu_Start.onClick.RemoveAllListeners ();
		Button_Menu_Start.onClick.AddListener (() => StartGame());
		Button_Menu_Quit.onClick.RemoveAllListeners ();
		Button_Menu_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));

		Button_Pause.onClick.RemoveAllListeners ();
		Button_Pause.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GamePaused));
		Button_Pause_BackToMenu.onClick.RemoveAllListeners ();
		Button_Pause_BackToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_Pause_Resume.onClick.RemoveAllListeners ();
		Button_Pause_Resume.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameResumed_AfterPause));
		Button_Pause_Restart.onClick.RemoveAllListeners ();
		Button_Pause_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterPause));

		Button_GameOver_GoToMenu.onClick.RemoveAllListeners ();
		Button_GameOver_GoToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_GameOver_Quit.onClick.RemoveAllListeners ();
		Button_GameOver_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));
		Button_GameOver_Restart.onClick.RemoveAllListeners ();
		Button_GameOver_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterLost));
	}

	void StartGame(){

		if (DataManager.Instance.oPlayerData.CurrentLevel.IsUnlocked) {
			globalEventsManager.TriggerOnChangeGameState (GameState.GameOn);
		}
	}

	void UpdateHud(int PointsToAdd){
		
		DataManager.Instance.oPlayerData.Points = DataManager.Instance.oPlayerData.Points + PointsToAdd;
		pointsText.text = "POINTS: "+DataManager.Instance.oPlayerData.Points.ToString ();
	}

	void GameplayContainerActive(bool shouldBeActive){
		if (shouldBeActive) {
			gamePlayPanel =Instantiate(Resources.Load<GameObject> ("Prefabs/Gameplays/GamePlayContainer"));
			gamePlayPanel.transform.SetParent (gamePlayPlaceHolder.transform);
		}
		if (!shouldBeActive) {
			Destroy(gamePlayPanel);
		}
	}
}
