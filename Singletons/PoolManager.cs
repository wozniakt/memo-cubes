﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject HeroesPool, EffectsPool, BulletsPool, PowerUpsPool, cubesPool;
	public bool WillGrow = true;


	List<GameObject> pooledHeroes;
	List<GameObject> pooledEffects;
	List<GameObject> pooledBullets;
	List<GameObject> pooledPowerUps;
	List<GameObject> cubesList;
	void Awake()
	{  
		instance = this;
		pooledEffects = new List<GameObject> ();
		pooledBullets = new List<GameObject> ();
		cubesList = new List<GameObject> ();
		pooledPowerUps = new List<GameObject> ();
	}
	void Start(){
		createCubes(25,"Cube");
		createEffects(5,EffectType.Explosion1);


	
		//createCubes (4, "Enemy1");
	}

	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}



	//


	//************************PowerUps
	void createPowerUps(int count, string powerUpName)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
			obj.SetActive(false);
			obj.name = powerUpName.ToString();
			pooledPowerUps.Add(obj);
			obj.transform.SetParent(PowerUpsPool.transform);
		}

	}

	public GameObject GetPooledObject_PowerUp(string powerUpName)
	{
		//		Debug.Log (POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString());
		foreach (GameObject item in pooledPowerUps)
		{
			if (item.name == powerUpName.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			//			///////Debug.Log (powerUpTypeNumber);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
			obj.name=powerUpName.ToString();
			pooledPowerUps.Add(obj);
			obj.transform.SetParent(PowerUpsPool.transform);
			return obj;
		}
		return null;
	}
	//************************Enemies
	void createCubes(int count, string cubeName)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Cubes/"+cubeName.ToString()));
			obj.SetActive(false);
			obj.name = cubeName.ToString();
			// 			Debug.Log (enemyName);
			cubesList.Add(obj);
			obj.transform.SetParent(cubesPool.transform);
		}

	}

	public GameObject GetPooledObject_Cubes(string cubeName)
	{
		foreach (GameObject item in cubesList)
		{
			if (item.name == cubeName.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{

			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Cubes/"+cubeName.ToString()));
			obj.name=cubeName.ToString();
			cubesList.Add(obj);
			obj.transform.SetParent(cubesPool.transform);
			return obj;
		}
		return null;
	}

}







