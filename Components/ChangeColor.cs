﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeColor : MonoBehaviour
{
	public float ChangeColorInterval;
	int randomNo;
	// Use this for initialization
	void Start()
	{
		randomNo= Random.Range(7,12);
		//GlobalEventsManager.instance.OnChangeGameState += this.ExplodeOnGameOver;
	}


	void OnEnable ()
	{
		GlobalEventsManager.instance.OnChangeGameState += this.ExplodeOnEndGame;
		DataManager.Instance.currentCubesCount++;

		if (ColorsManager.instance!=null & DataManager.Instance.oPlayerData.CurrentLevel!=null) {
			colorChangeRandomFromList (DataManager.Instance.oPlayerData.CurrentLevel.ColorsList);
			StartCoroutine (changeColorAfterTime (ChangeColorInterval));
		}
	}

	void OnDisable(){
		DataManager.Instance.currentCubesCount--;
		GlobalEventsManager.instance.OnChangeGameState -= this.ExplodeOnEndGame;
	}

	void colorChangeRandomFromList(List<Color> colorsList){
//		Debug.Log (colorsList.Count);
		Color thisColor =colorsList[Random.Range(0,colorsList.Count)];
		this.GetComponent<MeshRenderer> ().material.color = thisColor;
	}

	void colorChange(Color targetColor){
		this.GetComponent<MeshRenderer> ().material.color = targetColor;
	}
		
	void OnMouseUp ()
	{
		
		//Debug.Log ("name : " + this.GetComponent<MeshRenderer>().material.color+" color curr: "+ColorsManager.instance.currentColor);
		if (DataManager.Instance.oGameState==GameState.GameOn ||DataManager.Instance.oGameState==GameState.GameRestart_AfterLost||DataManager.Instance.oGameState==GameState.GameRestart_AfterPause ||DataManager.Instance.oGameState==GameState.GameResumed_AfterPause) {

		if (this.GetComponent<MeshRenderer>().material.color==ColorsManager.instance.currentColor) {
				Explode();
			//gameObject.SetActive (false);

			ColorsManager.instance.changeCurrentColor ();
				GlobalEventsManager.instance.TriggerGetPoints (1*DataManager.Instance.oPlayerData.CurrentLevel.PointsMulitipler);
				UI_Manager.instance.counterTime = UI_Manager.instance.counterTime + 0.2f;
				UI_Manager.instance.counterTime = Mathf.Min (UI_Manager.instance.counterTime, 1);
//			DistractionManager.instance.panelDistraction.SetActive (true);

				if (DataManager.Instance.oPlayerData.Points% randomNo==0) {
					randomNo= Random.Range(7,12);
				GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.Distraction);
			}
		} else if (this.GetComponent<MeshRenderer>().material.color!=Color.black) {
			GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.GameLost);
			} 
		}
	}

		IEnumerator changeColorAfterTime(float ChangeColorInterval){
			yield return new WaitForSeconds (Random.Range(ChangeColorInterval,2*ChangeColorInterval));
			colorChangeRandomFromList (ColorsManager.instance.fakeColorsList);

			yield return new WaitForSeconds (1);
			colorChangeRandomFromList (DataManager.Instance.oPlayerData.CurrentLevel.ColorsList);
//		Debug.Log ("!!!"+DataManager.Instance.oPlayerData.CurrentLevel.ColorsList.Count);
			StartCoroutine (changeColorAfterTime (ChangeColorInterval));
	}

		
	void ExplodeOnEndGame(GameState oGameState){
		
		if (oGameState==GameState.GameLost || oGameState==GameState.GameRestart_AfterPause || oGameState==GameState.GameRestart_AfterLost || oGameState==GameState.Menu) {
			SoundsManager.Instance.PlaySound (SoundType.Explode);
			SoundsManager.Instance.PlaySound (SoundType.laugh);
			GameObject explosion = PoolManager.instance.GetPooledObject_Effect (EffectType.Explosion1);
			explosion.transform.position = this.transform.position;
			explosion.GetComponent<ParticleSystem> ().startColor =new Color(this.GetComponent<MeshRenderer>().material.color.r,
			this.GetComponent<MeshRenderer>().material.color.g,this.GetComponent<MeshRenderer>().material.color.b);
			explosion.SetActive (true);
			this.gameObject.SetActive (false);

		}
	
	}
	void Explode(){
		SoundsManager.Instance.PlaySound (SoundType.Explode);
		//if (oGameState==GameState.GameLost || oGameState==GameState.GameRestart_AfterPause || oGameState==GameState.GameRestart_AfterLost || oGameState==GameState.Menu) {
			//Debug.Log (this.name + " " + "explode2"+" "+oGameState);
			GameObject explosion = PoolManager.instance.GetPooledObject_Effect (EffectType.Explosion1);
			explosion.transform.position = this.transform.position;
			explosion.GetComponent<ParticleSystem> ().startColor =new Color(this.GetComponent<MeshRenderer>().material.color.r,
				this.GetComponent<MeshRenderer>().material.color.g,this.GetComponent<MeshRenderer>().material.color.b);
			explosion.SetActive (true);
			this.gameObject.SetActive (false);

		//}

	}
}

