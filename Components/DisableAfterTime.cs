﻿using UnityEngine;
using System.Collections;
using System;

public class DisableAfterTime : MonoBehaviour
{
	public float timeToDisable;
	// Use this for initialization
	void OnEnable ()
	{
		//timeToDisable = 0;
		//		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		StartCoroutine (disable ());
	}

	// Update is called once per frame
	IEnumerator disable ()
	{
		yield return new WaitForSeconds (timeToDisable);
		this.gameObject.SetActive (false);

	}
}


