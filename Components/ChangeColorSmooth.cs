﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class ChangeColorSmooth : MonoBehaviour
{
	public MeshRenderer meshRend;
	public Image imgRend;
	public Color lerpedColor = Color.white;
	public List<Color> colors;
	// Use this for initialization
	void Start ()
	{
		meshRend = GetComponent<MeshRenderer> ();
		if (meshRend!=null) {
			StartCoroutine (ChangeColorsSmoothly (colors));
		}
//		if (imgRend!=null) {
//			StartCoroutine (ChangeColorsSmoothlyImage (colors));
//		}

	}

	IEnumerator ChangeColorsSmoothly(List<Color> colorsList){
			for (int j = 0; j < colorsList.Count-1; j++) {
				for (float i = 0; i < 1; i=i+0.01f) {
					meshRend.material.color= Color.Lerp(colorsList[j], colorsList[j+1], i);
					yield return new WaitForSeconds(0.01f);
				}
			}
		StartCoroutine (ChangeColorsSmoothly (colors));
	}


	IEnumerator ChangeColorsSmoothlyImage(List<Color> colorsList){
		for (int j = 0; j < colorsList.Count-1; j++) {
			for (float i = 0; i < 1; i=i+0.01f) {

				imgRend.GetComponent<Image>().color= Color.Lerp(colorsList[j], colorsList[j+1], i);
				yield return new WaitForSeconds(0.01f);
			}
		}
		StartCoroutine (ChangeColorsSmoothlyImage (colors));
	}


}

