﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkyboxChange : MonoBehaviour
{
	public List<Material> skyboxes;
	// Use this for initialization
	void Start ()
	{
		GlobalEventsManager.instance.OnGetPoints += this.ChangeSky;
		RenderSettings.skybox=skyboxes[Random.Range(0,skyboxes.Count)];
	}

	void OnDisable(){
		GlobalEventsManager.instance.OnGetPoints += this.ChangeSky;
	}

	void ChangeSky (int blankPoints)
	{
		if (DataManager.Instance.oPlayerData.Points%5==0) {

			RenderSettings.skybox = skyboxes [Random.Range (0, skyboxes.Count)];
		}
	}
}

