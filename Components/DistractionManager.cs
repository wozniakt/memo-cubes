﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class DistractionManager : MonoBehaviour
{

	public int number1, number2, result, fakeResult, fakeResult2;
	public List<Button> buttonsList=new List<Button>();
	public GameObject panelDistraction;
	public int tempNumber1,tempNumber2,tempNumber3;
	public Text txtNumbers;
	// Use this for initialization
	public static DistractionManager instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}

	void OnEnable ()
	{
		randomizeNumbers ();
		txtNumbers.text = number1.ToString ()+" + "+number2.ToString ()+" = ";
		RandomizeResultsAmongButtons ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void RandomizeResultsAmongButtons(){
		List<Button> tmpButtonsList = new List<Button>(buttonsList);
		//Debug.Log (tempNumber+"   "+tmpButtonsList.Count);
		tempNumber1 = Random.Range (0,tmpButtonsList.Count);
		buttonsList [tempNumber1].GetComponentInChildren<Text> ().text = result.ToString();
		buttonsList [tempNumber1].onClick.RemoveAllListeners ();
		buttonsList [tempNumber1].onClick.AddListener (() => ClickResult(result));
		tmpButtonsList.RemoveAt(tempNumber1);
//		Debug.Log (tempNumber1+"   "+tmpButtonsList.Count);
		int counter = 0;

		foreach (Button oButton in tmpButtonsList) {
			
			oButton.GetComponentInChildren<Text> ().text = (fakeResult+counter).ToString();
			oButton.onClick.RemoveAllListeners ();
			oButton.onClick.AddListener (() => ClickResult(fakeResult+counter));
			 counter = -2;
		}
		
	}

	void ClickResult(int resultClicked){
		//Debug.Log (result + "  " + resultClicked);
		if (resultClicked==result) {
			//Debug.Log (result + "  " + resultClicked+" "+resultClicked!=result);
			GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.GameResumed_AfterPause);
			randomizeNumbers ();
			txtNumbers.text = number1.ToString ()+" + "+number2.ToString ()+" = ";
			RandomizeResultsAmongButtons ();
			UI_Manager.instance.counterTime = 1;
			panelDistraction.SetActive (false);

		} else {
			if (resultClicked!=result) {
				//Debug.Log (result + "  " + resultClicked+" "+resultClicked!=result);
				GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.GameLost);
				randomizeNumbers ();
				txtNumbers.text = number1.ToString ()+" + "+number2.ToString ()+" = ";
				RandomizeResultsAmongButtons ();

				//Debug.Log (result + " " + resultClicked);
			}
		}

	}
	void randomizeNumbers(){
		number1 = Random.Range (6, 11);
		number2 = Random.Range (6, 11);
		result = number1+number2;
		fakeResult = number1 + number2 + 1;
		fakeResult2 = number1 + number2 - 1;

	}
}

