﻿using UnityEngine;
using System.Collections;

public class RandomizePosition : MonoBehaviour
{
	public Vector3 initialPosition;
	public Vector3 initialRotation;
	public GameObject target;

	// Use this for initialization
	void Start ()
	{
		GlobalEventsManager.instance.OnGetPoints += this.ChangePosition;
		GlobalEventsManager.instance.OnChangeGameState += this.ResetPosition;
		initialPosition = this.transform.position;
		initialRotation = this.transform.eulerAngles;
	//	Debug.Log (initialRotation);

	}

	void OnDisable(){
		
		GlobalEventsManager.instance.OnChangeGameState -= this.ResetPosition;
	}

	void ResetPosition(GameState oGameState){
		if (oGameState==GameState.GameLost) {
			this.transform.position = initialPosition;
			this.transform.eulerAngles = initialRotation;
		}
	}
	void Update(){
		
	}


	void ChangePosition(int blank){
		StartCoroutine (RandomPosition());
	}
	// Update is called once per frame
	IEnumerator RandomPosition ()
	{
		yield return new WaitForSeconds (0.2f);
		this.transform.position = new Vector3 (
			Random.Range(initialPosition.x-20,initialPosition.x+20),
			Random.Range(initialPosition.y-5,initialPosition.y+20),
			Random.Range(initialPosition.z-5,initialPosition.z+20));
		
		this.transform.eulerAngles= 	( new Vector3 (
			Random.Range(initialRotation.x-100,initialRotation.x+100),
			Random.Range(initialRotation.y-100,initialRotation.y+100),
			Random.Range(initialRotation.z,initialRotation.z)
		));

//		Debug.Log (this.transform.eulerAngles.z);
		this.transform.LookAt(target.transform);
		this.transform.eulerAngles= 	( new Vector3 (
			Random.Range(transform.eulerAngles.x,transform.eulerAngles.x),
			Random.Range(transform.eulerAngles.y,transform.eulerAngles.y),
			Random.Range(initialRotation.z,initialRotation.z)
		));
//		Debug.Log (this.transform.eulerAngles.z);

	}
}

