﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ObjectsSpawner : MonoBehaviour
{
	public List<GameObject> CubesList;
	public float intervalSingleCube, intervalGroupCubes;
	public int cubesInGroup;
	 GameObject cube;
	public bool isStartingPoint;//musi być zaznaczony jeśli mamy używać poniższej zmiennej:
	public GameObject startingPoint;
	public bool startAtGroup;
	public int horizontalInt, verticalInt;
	GlobalEventsManager globalEventsManager;

	void Start ()
	{
		
		globalEventsManager = GlobalEventsManager.instance;
		GlobalEventsManager.instance.OnGetPoints += this.SpawnCubesOnPointsChange;
		StartCoroutine (SpawnCubes ( intervalSingleCube,intervalGroupCubes));
	}
	void OnDisable(){
		GlobalEventsManager.instance.OnGetPoints -= this.SpawnCubesOnPointsChange;
	}

	void SpawnCubesOnPointsChange(int points){
		StartCoroutine (SpawnCubes ( intervalSingleCube,intervalGroupCubes));
	}


	// Update is called once per frame
	public IEnumerator SpawnCubes (float intervalSingleGroup,float intervalGroupCubes)
	{
		if (DataManager.Instance.currentCubesCount<18) {
			

		for (int i = 0; i < cubesInGroup*DataManager.Instance.oPlayerData.CurrentLevel.ColorsList.Count; i++) {

			cube = PoolManager.instance.GetPooledObject_Cubes ((CubesList[Random.Range(0,CubesList.Count)]).name.ToString());

			if (isStartingPoint==true) {
				if (startAtGroup) {
					cube.transform.position =  new Vector3( startingPoint.transform.position.x+(i*horizontalInt),
						this.transform.position.y+(i*verticalInt),10);
				} else {

					cube.transform.position =  new Vector3( startingPoint.transform.position.x,
						this.transform.position.y,10);
				}
			} else {
				cube.transform.position = new Vector3( this.transform.position.x+Random.Range(-3,3),
					this.transform.position.y+Random.Range(-3,3),this.transform.position.z+Random.Range(-5,5));
			}
			cube.SetActive (true);
			yield return new WaitForSeconds (intervalSingleGroup);
		}
		yield return new WaitForSeconds (intervalGroupCubes);
		//StartCoroutine (SpawnCubes (intervalSingleGroup,intervalGroupCubes));
		}
	}
}

